import { PlayerId } from "./PlayerDTO";

export interface PlayerAnswerDTO<AnswerType> {
    playerId: PlayerId,
    vote: AnswerType
}
