import { GameDTO } from "./GameDTO";
import { PartyStatus } from "./PartyStatus";
import { PlayerDTO } from "./PlayerDTO";

export interface PartyDTO {
    id: string;
    name: string;
    roomCode: string;
    players: PlayerDTO[];
    maxPlayers: number;
    status: PartyStatus;
    currentGame?: GameDTO;
}
