import { BasicGameStatus } from "./BasicGameStatus";
import { PlayerAnswerDTO } from "./PlayerAnswerDTO";
import { PlayerId } from "./PlayerDTO";

export interface GameDTO {
    id: string;
    answers: PlayerAnswerDTO<AnswerType>[];
    status: BasicGameStatus;
    answerType: AnswerTypeEnum;
    content: any;
    name: string;
    description?: string;
}

export type AnswerType = string | PlayerId | number
export enum AnswerTypeEnum {
    PlayerId,
    Number,
    String
}
