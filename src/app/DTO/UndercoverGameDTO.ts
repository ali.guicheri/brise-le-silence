import { GameCardDTO } from "./GameCardDTO"
import { GameDTO } from "./GameDTO"
import { PlayerId } from "./PlayerDTO"
import { UnderCoverStatusEnum } from "./UndercoverStatus"

export interface UndercoverGameDTO extends GameDTO {
    content: {
        playersRole: Map<PlayerId, GameCardDTO>,
        underCoverStatus: UnderCoverStatusEnum
    }
}
