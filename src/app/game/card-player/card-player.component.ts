import { Component } from '@angular/core';
import { PartyDTO } from 'src/app/DTO/PartyDTO';

@Component({
  selector: 'app-card-player',
  templateUrl: './card-player.component.html',
  styleUrls: ['./card-player.component.scss']
})

export class CardPlayerComponent {
  public party!:PartyDTO;

}
