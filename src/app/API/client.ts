
import { io } from "socket.io-client"
import type API from "./MainApi";
import { initAPI } from "../lib/QuickApi";

const socket = io("http://172.70.8.10:3000/");

export const { callAPI } = initAPI<typeof API>((endpoint, ...args) => {
  return new Promise((res, rej) => {
    const idResponse = crypto?.randomUUID?.() ?? Math.random();
    socket.emit("api", endpoint, idResponse, args);
    const clear = () => {
      socket.removeAllListeners(`${idResponse}@success`);
      socket.removeAllListeners(`${idResponse}@error`);
    }
    socket.on(`${idResponse}@success`, (result) => {
      res(result);
      clear();
    })
    socket.on(`${idResponse}@error`, (err) => {
      rej(err);
      clear();
    })
  })
});


