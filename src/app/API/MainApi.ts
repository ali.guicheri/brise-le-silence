import { AnswerType, GameDTO } from "../DTO/GameDTO";
import { PartyDTO } from "../DTO/PartyDTO";
import { PlayerAnswerDTO } from "../DTO/PlayerAnswerDTO";
import { PlayerDTO } from "../DTO/PlayerDTO";
import { API_DECLARATION } from "../lib/QuickApi.js";

export default {
    party: {
        join: (req, roomCode: string): PartyDTO => {
          //@ts-ignore
          return;
        },
        leave: (req): PartyDTO => {
          //@ts-ignore
          return;
        },
        get: (req): PartyDTO => {
          //@ts-ignore
          return;
        },
    },

    player: {
        setName: (req, name: string): PlayerDTO => {
          //@ts-ignore
          return;
        },
        vote: (req, content: AnswerType): PlayerAnswerDTO<any> => {
          //@ts-ignore
          return;
        },
        get: (req): PlayerDTO => {
          //@ts-ignore
          return;
        },
        getById: (_req, playerId: string): PlayerDTO => {
          //@ts-ignore
          return;
        },
    },

    game: {
        get: (req): GameDTO => {
          //@ts-ignore
          return;
        },

        answer: (req, content: AnswerType): PlayerAnswerDTO<any> => {
          //@ts-ignore
          return;
        }
    },




    // Accessible only to Presentation
    pres: {
        party: {
            create: (req, name: string, maxPlayers: number = 5) => {
          //@ts-ignore
          return;
            },
            start: (req) => {
          //@ts-ignore
          return;
            },
        }
    }

} as const satisfies API_DECLARATION

/** ENDPOIT A ECOUTER FRONT JOUEUR / le fil d'une parie
 * Party:Update -> (PartyDTO) // Changement d'état de la partie [Inlut l'ajout / supression de joueurs]
 * Game:Update -> (GameDTO) // Définition du jeu (Quizz / Undercover)
 * Game:Start -> (GameDTO + content) // Début du jeu
 *              Pour le undercover regarder UnderCoverGameDTO
 *              Pour le Quizz regarder QuizzGameDTO
 * Game:status:Answer -> (GameDTO + content) // Définit le début d'une phase de vote
 * Game:status:Stats -> (GameDTO + content) //La phrase par rapport à la situation [Pas forcément utilisée]
 * /////// Peut reboucler sur Game:Update
 * Game:End -> (GameDTO) // Fin du jeu
 *
 *
 *
 */
