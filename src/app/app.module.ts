import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardPlayerComponent } from './game/card-player/card-player.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { GameComponent } from './game/game.component';
import { QuestionComponent } from './game/question/question.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    CardPlayerComponent,
    HeaderComponent,
    LoginComponent,
    GameComponent,
    QuestionComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
