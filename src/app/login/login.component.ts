import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { callAPI } from '../API/client';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private router: Router) {}

  public redirect():void{
    console.log("oui");
    callAPI("party.join", "77bc6")
     this.router.navigateByUrl("game");
  }
}
